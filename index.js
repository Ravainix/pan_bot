const fs = require("fs");
// const snekfetch = require("snekfetch");
// const ytdl = require("ytdl-core");
// const nodeopus = require('node-opus');

const Discord = require("discord.js");
const { token, prefix } = require("./config/config.json");

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync("./commands");

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.name, command);
}

client.on("ready", () => {
  console.log("Ready!");
});

// client.on('message', message => {
//   if (message.content === '!play') {
//       if (message.channel.type !== 'text') return;

//       const { voiceChannel } = message.member;

//       if (!voiceChannel) {
//           return message.reply('please join a voice channel first!');
//       }

//       voiceChannel.join().then(connection => {
//           const stream = ytdl('https://www.youtube.com/watch?v=fEf-moUFxkQ', { filter: 'audioonly' });
//           const dispatcher = connection.playStream(stream);
//           process.on('unhandledRejection', error => console.error(`Uncaught Promise Rejection:\n${error}`));

//           dispatcher.on('end', () => voiceChannel.leave());
//       });
//   }
// });

client.on("message", message => {
  if (!message.content.startsWith(prefix) || message.author.bot) return;

  const args = message.content.slice(prefix.length).split(/ +/);
  const commandName = args.shift().toLowerCase();

  if (!client.commands.has(commandName)) return;

  const command = client.commands.get(commandName);

  try {
    // client.commands.get(command).execute(message, args);
    command.execute(message, args);
    console.log(`${message.author.username} used command ${commandName}`);
  } catch (error) {
    console.error(error);
    message.reply("there was an error trying to execute that command!");
  }
});

client.login(token);

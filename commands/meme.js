const snekfetch = require("snekfetch");

module.exports = {
  name: "meme",
  description: "test",
  args: true,
  execute(message) {
     try {
       snekfetch.get("https://memegen.link/api/templates/")
      .then(r =>  message.channel.send(r.body)); 
    } catch(err) {
      message.channel.send(err);
    }
  }
}
const ytdl = require("ytdl-core");

module.exports = {
  name: "play",
  description: "Play your music from youtube",
  args: true,
  execute(message, args) {
    if (message.channel.type !== "text") return;

    const { voiceChannel } = message.member;

    // if(!message.member.roles.some(r=>["Administrator", "Moderator"].includes(r.name)) )
    //   return message.reply("Sorry, you don't have permissions to use this!");

    if (!voiceChannel) {
      return message.reply("please join a voice channel first!");
    }

    if (!args[0]) {
      return message.reply("Dawaj link mordo");
    }

    voiceChannel.join().then(connection => {
      const stream = ytdl(args[0], { filter: "audioonly" });
      const dispatcher = connection.playStream(stream);

      dispatcher.on("end", () => voiceChannel.leave());
    });
  }
};
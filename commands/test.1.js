const snekfetch = require('snekfetch');

module.exports = {
  name: 'test1',
  description: 'test',
  args: false,
  execute(message) {
    const test = async () => {
      try {
        const { body } = await snekfetch.get('https://random.cat/meow');
        message.channel.send(body.file);
      } catch(err) {
        // console.log(err);
        message.channel.send('po calosci sie spierdolilo');
      }
    }
    test();
  }
}